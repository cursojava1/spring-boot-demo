package com.example.demo.controller;

import com.example.demo.entity.Billionaire;
import com.example.demo.service.BillionaireService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Gino Junchaya
 */

@RestController
@RequestMapping("/billionaires")
public class BillionaireController {
            
    @Autowired
    private BillionaireService billionaireService;
    
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findAll(
        @RequestParam(value = "career", required = false) String career, 
        @RequestParam(value = "first_name", required = false) String firstName,
        @RequestParam(value = "last_name", required = false) String lastName
    ){
        List<Billionaire> billionaires = billionaireService.findAll(career, firstName, lastName);
        return new ResponseEntity<>(billionaires, HttpStatus.OK);
    }
    
    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    public ResponseEntity findById(@PathVariable("id") Integer id){       
        Optional<Billionaire> billionaire = billionaireService.findById(id);
        if(billionaire.isPresent()){
            return new ResponseEntity<>(billionaire.get(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity("El billonario " + id + " no existe", HttpStatus.NOT_FOUND);
        }
    }
   
    @DeleteMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    public ResponseEntity delete(@PathVariable("id") Integer id){
        Boolean deleted = billionaireService.delete(id);
        if(deleted){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("El billonario " + id + " no existe", HttpStatus.NOT_FOUND);
        }
    }
    
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestBody Billionaire entity){
        Billionaire saved = billionaireService.save(entity);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }
    
}
