package com.example.demo.repository;

import com.example.demo.entity.Billionaire;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gino Junchaya
 */
@Repository
public interface BillionaireRepository extends JpaRepository<Billionaire, Integer>{
    
    @Query("SELECT B FROM Billionaire B WHERE "
        + "(:career IS NULL OR B.carrer = :career) AND "
        + "(:first_name IS NULL OR B.firstName = :first_name) AND "
        + "(:last_name IS NULL OR B.lastName = :last_name)")
    List<Billionaire> findByFilters(
        @Param("career") String career, 
        @Param("first_name") String firstName, 
        @Param("last_name") String lastName
    );
    
}