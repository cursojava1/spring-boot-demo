package com.example.demo.service;

import com.example.demo.entity.Billionaire;
import com.example.demo.repository.BillionaireRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gino Junchaya
 */
@Service
public class BillionaireService {
        
    @Autowired
    private BillionaireRepository billionaireRepository;

    public List<Billionaire> findAll(String career, String firstName, String lastName){        
        return billionaireRepository.findByFilters(career, firstName, lastName);
    }
    
    public Optional<Billionaire> findById(Integer id){
        return billionaireRepository.findById(id);
    }
    
    public Boolean delete(Integer id){
        Optional<Billionaire> billionaire = findById(id);
        if(billionaire.isPresent()){
            billionaireRepository.deleteById(id);
            return Boolean.TRUE;
        }
        else {
            return Boolean.FALSE;
        }
    }
        
    public Billionaire save(Billionaire entity){
        return billionaireRepository.save(entity);
    }
    
}
